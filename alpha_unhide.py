from PIL import Image
import binascii
from optparse import OptionParser

def isTransparentPixel(img, x: int, y: int) -> bool:
    (r, g, b, a) = img.getpixel((x,y))
    if(a == 0):
        return True
    else:
        return False

def unhide(imgName):
    img = Image.open(imgName)
    input("USER")
    #contains available pixels for hiding
    AVAILABLE = []
    #check for transparent pixels
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            if(isTransparentPixel(img, i, j)):
                AVAILABLE.append((i, j))
    i = 0
    data = ""
    coord = AVAILABLE[0]
    x = coord[0]
    y = coord[1]
    (r,g,b,a) = img.getdata().getpixel((x,y))
    i += 1
    while((r,g,b,a) != (0, 0, 0,0)):
        data += chr(r)
        data += chr(g)
        data += chr(b)
        coord = AVAILABLE[i]
        x = coord[0]
        y = coord[1]
        (r, g, b, a) = img.getpixel((x,y))
        i += 1
    return data


def get_options():
    parser = OptionParser()
    # required
    parser.add_option("-f", "--inputfile", type="string", help="Input file in witch data is hidden.")
    # Optionals
    parser.add_option("-o", "--outputfile", type="string", help="Name of the output file to write the hidden data to.")
    (options, args) = parser.parse_args()
    if len(args) != 0 or not options.inputfile:
        parser.print_help()
        raise SystemExit
    return options

if __name__ == '__main__':
    options = get_options()
    data = unhide(options.inputfile)
    if options.outputfile:
        with open(options.outputfile, "w+") as f:
            f.write(data.replace("\n",""))
    else:
        print(data)
