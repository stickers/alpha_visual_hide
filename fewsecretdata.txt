A thousand signs declined
That traveled through light
Translate this mystery
That covered my eyes
Accept approaching fear
And courage appears
Death is a certainty
It's growing near
Letting go is fateful
Uniting broken rhymes
