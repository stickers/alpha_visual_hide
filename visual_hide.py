from PIL import Image
from random import choice
from optparse import OptionParser

def pixelNumberToCoordinate(n, img):
    """
    Converts pixel number to coordinates.
    Ex: Image size is w=10, h=20
    The 23th pixel has coordinates of (3, 2).
    """
    return (n%img.size[0], n//img.size[0])

def coordinateToPixelNumber(x, y, img):
    """
    Converts coordinates to pixel number.
    Ex: Image size is w=10, h=20
    pixel (3, 2) is the 23th pixel.
    """
    return int(y*img.size[0]+x)

def isSafePixel(img, x: int, y: int, BLOCKLEN: int) -> bool:
            width = img.size[0]
            height = img.size[1]
            #Checks if pixel is not out of bounds of image
            if(x < 0):
                return False
            if(y < 0):
                return False
            if(x > width - BLOCKLEN - 1):
                return False
            if(y > height - BLOCKLEN - 1):
                return False
            #check directions, different code needed if alpha channel is included or not
            if(img.mode == 'RGBA'):
                #up, left, down, right
                ur, ug, ub, au = img.getpixel((x,y-1))
                lr, lg, lb, al = img.getpixel((x-1,y))
                dr, dg, db, ad = img.getpixel((x,y+1))
                rr, rg, rb, ar = img.getpixel((x+1,y))
                #right 1 pixel, right 2 pixel...up to right 10 pixels
                r1, g1, b1, a1 = img.getpixel((x+1,y))
                r2, g2, b2, a2 = img.getpixel((x+2,y))
                r3, g3, b3, a3 = img.getpixel((x+3,y))
                r4, g4, b4, a4 = img.getpixel((x+4,y))
                r5, g5, b5, a5 = img.getpixel((x+5,y))
                r6, g6, b6, a6 = img.getpixel((x+6,y))
                r7, g7, b7, a7 = img.getpixel((x+7,y))
                r8, g8, b8, a8 = img.getpixel((x+8,y))
                r9, g9, b9, a9 = img.getpixel((x+9,y))
                r10, g10, b10, a10 = img.getpixel((x+10, y))
            else:
                ur, ug, ub = img.getpixel((x,y-1))
                lr, lg, lb = img.getpixel((x-1,y))
                dr, dg, db = img.getpixel((x,y+1))
                rr, rg, rb = img.getpixel((x+1,y))
                r1, g1, b1 = img.getpixel((x+1,y))
                r2, g2, b2 = img.getpixel((x+2,y))
                r3, g3, b3 = img.getpixel((x+3,y))
                r4, g4, b4 = img.getpixel((x+4,y))
                r5, g5, b5 = img.getpixel((x+5,y))
                r6, g6, b6 = img.getpixel((x+6,y))
                r7, g7, b7 = img.getpixel((x+7,y))
                r8, g8, b8 = img.getpixel((x+8,y))
                r9, g9, b9 = img.getpixel((x+9,y))
                r10, g10, b10 = img.getpixel((x+10, y))

            #Obtain lsb of positions
            lsb_r1 = r1 & 1
            lsb_g1 = g1 & 1
            lsb_b1 = b1 & 1
            lsb_r2 = r2 & 1
            lsb_g2 = g2 & 1
            lsb_b2 = b2 & 1
            lsb_r3 = r3 & 1
            lsb_g3 = g3 & 1
            lsb_b3 = b3 & 1
            lsb_r4 = r4 & 1
            lsb_g4 = g4 & 1
            lsb_b4 = b4 & 1
            lsb_r5 = r5 & 1
            lsb_g5 = g5 & 1
            lsb_b5 = b5 & 1
            lsb_r6 = r6 & 1
            lsb_g6 = g6 & 1
            lsb_b6 = b6 & 1
            lsb_r7 = r7 & 1
            lsb_g7 = g7 & 1
            lsb_b7 = b7 & 1
            lsb_r8 = r8 & 1
            lsb_g8 = g8 & 1
            lsb_b8 = b8 & 1
            lsb_r9 = r9 & 1
            lsb_g9 = g9 & 1
            lsb_b9 = b9 & 1
            lsb_r10 = r10 & 1
            lsb_g10 = g10& 1
            lsb_b10= b10 & 1
            lsb_ug = ug & 1
            lsb_ub = ub & 1
            lsb_ur = ur & 1
            lsb_lr = lr & 1
            lsb_lg = lg & 1
            lsb_lb = lb & 1
            lsb_dr = dr & 1
            lsb_dg = dg & 1
            lsb_db = db & 1
            lsb_rr = rr & 1
            lsb_rg = rg & 1
            lsb_rb = rb & 1
            #If pixel is the same as pixel above it, to the left, to the right and to the bottom, not safe
            if((lsb_ur is lsb_lr is lsb_dr is lsb_rr) and (lsb_ug is lsb_lg is lsb_dg is lsb_rg) and (lsb_ub is lsb_lb is lsb_db is lsb_rb)):
                return False
            #If the first pixel is the same as second pixel to the right, or the third pixel is the same as the 4th pixel to the rigth...up to 10th pixel
            #not safe
            if(((lsb_r1 is lsb_r2) or (lsb_r3 is lsb_r4) or (lsb_r5 is lsb_r6) or (lsb_r7 is lsb_r8) or (lsb_r9 is lsb_r10))
               and((lsb_g1 is lsb_g2) or (lsb_g3 is lsb_g4) or (lsb_g5 is lsb_g6) or (lsb_g7 is lsb_g8) or (lsb_g9 is lsb_g10))
               and((lsb_b1 is lsb_b2) or (lsb_b3 is lsb_b4) or (lsb_b5 is lsb_b6) or (lsb_b7 is lsb_b8) or (lsb_b9 is lsb_b10))):
                return False
            else:
                return True

def filterAvailable(AVAILABLE, BLOCKLEN):
    """
    Takes a list of available coordinates for LSB manipulation, and removes coordinates
    which are closer then BLOCK_LEN to each other.
    This is necessary because each available pixel will have a block appended to it,
    which must not overlap
    """
    AVAILABLE = sorted(AVAILABLE)
    removedlist = []
    removedlist.append(AVAILABLE[0])
    lastappended = AVAILABLE[0]
    i = 1
    while(i < len(AVAILABLE)):
        curr = AVAILABLE[i]
        if(curr >= lastappended + BLOCKLEN):
            lastappended = curr
            removedlist.append(curr)
            i = i + 1
        else:
            i = i + 1
    return removedlist

def setLSB(v, state):
    if state == "0":
        return v & 0b11111110
    elif state == "1":
        return v | 0b00000001
    else:
        print(f"invalide state: {state}")
        return v

def write(data, pixel, nextP, img):
    """
    Writes a block of data and pointer to the next pixel in binary format at a given pixel.

    @param data: Binary representation of a block of data.
    @type data: String

    @param pixel: Pixel number where the block starts.
    @type pixel: Int

    @param nextP: Pixel number to the next block of data.
    @type nextP: Int

    @param img: Image Object.
    @type img: Image
    """
    pix = img.load()
    x, y = pixelNumberToCoordinate(nextP, img)
    l = len(data)
    # binari representation of next pixel x
    col = bin(x)[2:].zfill(l)
    # binari representation of next pixel y
    lin = bin(y)[2:].zfill(l)

    for i in range(pixel, pixel+l):
        p = pix[pixelNumberToCoordinate(i, img)]
        if len(p) == 4:
            # With alpha channel
            pix[pixelNumberToCoordinate(i, img)] = (
            setLSB(p[0], data[i-pixel]),
            setLSB(p[1], col[i-pixel]),
            setLSB(p[2], lin[i-pixel]),
            p[3])
        else:
            # no alpha channel
            pix[pixelNumberToCoordinate(i, img)] = (
            setLSB(p[0], data[i-pixel]),
            setLSB(p[1], col[i-pixel]),
            setLSB(p[2], lin[i-pixel]))

def toBin(string):
    return ''.join(format(x, 'b').zfill(8) for x in string)

def chunkstring(string, length):
    return [string[0+i:length+i].ljust(length, "0") for i in range(0, len(string), length)]

def hide(data, imgName, outName, startingPixel=(0,0)):
    """
    Hides the string data in the image imgName and creates a new image containing the data outName.
    startingPixel is optional and will be choosed randomly if not specified.

    @param data: Data to hide.
    @type data: String

    @param imgName: Name of the original image.
    @type imgName: String

    @param outName: Name of the resulting image.
    @type outName: String

    @param startingPixel: Optional starting pixel coordinates.
    @type startingPixel: Tuple

    @returns: The starting pixel used.
    @rtype: Tuple
    """
    input("USER")
    img = Image.open(imgName)
    BLOCKLEN = len(bin(max(img.size))[2:])
    total = img.size[0] * img.size[1]

    #Check for pixels which can hide data, done by isSafePixel function, they are stored in list AVAILABLE
    AVAILABLE = []
    for i in range(img.size[0]):
        for j in range(img.size[1]):
            if(isSafePixel(img, i, j, BLOCKLEN)):
                AVAILABLE.append(coordinateToPixelNumber(i, j, img))
    input("USER")
    #If no available pixels, exit
    if (len(AVAILABLE)==0):
        print("No Available pixels to hide")
        raise SystemExit
    #Ensure that each available pixels are at least BLOCKLEN apart, so that blocks can not overlap
    else:
        AVAILABLE = filterAvailable(AVAILABLE, BLOCKLEN)

    
    # Check if the last position is big enough
    if AVAILABLE[-1] + BLOCKLEN >= total:
        AVAILABLE.pop()

    #Check if enough capacity
    if(len(AVAILABLE) < len(toBin(data))):
       raise SystemExit

    #grabs chunk of data to hide
    d = chunkstring(toBin(data),BLOCKLEN)
    n = len(d)
    # choose the first pixel
    pixel = coordinateToPixelNumber(startingPixel[0], startingPixel[1], img)
    if pixel == 0:
        # Choose a random location because (0, 0) is not authorized
        pixel = choice(AVAILABLE)
        AVAILABLE.remove(pixel)
        startingPixel = pixelNumberToCoordinate(pixel, img)
    for i in range(n-1):
        # pointer to the next pixel
        nextP = choice(AVAILABLE)
        AVAILABLE.remove(nextP)
        write(d[i], pixel, nextP, img)
        # switch to next pixel
        pixel = nextP
    # last pointer towards NULL (0, 0)
    write(d[-1], pixel, 0, img)
    img.save(outName)
    img.close()
    return startingPixel

def get_options():
    parser = OptionParser()
    # required
    parser.add_option("-f", "--inputfile", type="string", help="Input file in witch data should be hidden.")
    parser.add_option("-d", "--data", type="string", help="Data (represented as string) to hide.")
    parser.add_option("-s", "--secretfile", type="string", help="Secret file to hide.")
    # Optionals
    parser.add_option("-o", "--outputfile", type="string", default="out.png", help="Name of the output file containing the hidden data.")
    parser.add_option("-x", type=float, help="Starting pixel's x coordinate.")
    parser.add_option("-y", type=float, help="Starting pixel's y coordinate.")
    (options, args) = parser.parse_args()

    if len(args) != 0 or not options.inputfile or (not options.data and not options.secretfile):
        parser.print_help()
        raise SystemExit

    if options.secretfile and options.data:
        print("Only one of --secretfile (-s) or --data (-d) should be provided.")
        parser.print_help()
        raise SystemExit

    if options.secretfile:
        with open(options.secretfile, "rb") as f:
            options.data = f.read()

    # force bytes
    if type(options.data) == str:
        options.data = options.data.encode()

    return options

if __name__ == '__main__':
    options = get_options()
    input("USER")
    if options.x and options.y:
        print(hide(options.data, options.inputfile, options.outputfile, (options.x, options.y)))
    else:
        print(hide(options.data, options.inputfile, options.outputfile))
