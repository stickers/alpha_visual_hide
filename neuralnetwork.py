from steganogan import SteganoGAN
from steganogan.critics import BasicCritic
from steganogan.decoders import DenseDecoder
from steganogan.encoders import DenseEncoder
from steganogan.loader import DataLoader

val = DataLoader('/vol/tensusers4/jboshove/projectns/data/val')
train = DataLoader('/vol/tensusers4/jboshove/projectns/data/train')

steganogan = SteganoGAN(1, DenseEncoder, DenseDecoder, BasicCritic, hidden_size=32, cuda=True, verbose=True)

steganogan.fit(train, val, epochs=20)

steganogan.save('/vol/tensusers4/jboshove/projectns/models/dense.steg')

steganogan = SteganoGAN.load('/vol/tensusers4/jboshove/projectns/models/dense')

steganogan.encode('/vol/tensusers4/jboshove/projectns/data/train/1/STK-20190526-WA0009.png', '/vol/tensusers4/jboshove/projectns/out.png', 'THis is a secret message')

print(steganogan.decode('/vol/tensusers4/jboshove/projectns/out.png'))
