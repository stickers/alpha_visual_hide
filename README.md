# alpha_visual_hide

TXT.files <br>
	secretdata.txt contains original data <br>
	fewsecretdata.txt contains less data <br>
	lotsecretdata.txt contains a lot of data

VISUAL_HIDE.PY VISUAL_UNHIDE.PY <br>
	visual_hide.py and visual_unhide.py implement our steganography method which can escape visual inspection

	ISSUES:
	-The same starting position needs to be specified for visual_hide.py and visual_unhide.py, it is not automated yet
	-Additional capacity may be obtained by altering isSafePixel function

	USAGE
	.\visual_hide.py -f test.png -s b.txt -x 300 -y 300 -o out.png

	.\visual_unhide.py -f out.png -x 300 -y 300 -o out.txt

ALPHA_HIDE.PY ALPHA_UNHIDE.PY <br>
	alpha_hide.py and alpha_unhide.py hide bits behind existing transparency

	ISSUES:
	When hiding the bits, if the total length of data is not a multiple of 3, the last bits are not encoded.
	To solve this, a manual check has to be performed to make sure the last bits are not extracted if they do not exist.
	The extraction is done in this piece of code in the hide function:
	r = int(bits_to_hide[i], 2)
        g = int(bits_to_hide[i+1], 2) #This can not exist if not multiple of 3
        b = int(bits_to_hide[i+2], 2) #This can not exist if not multiple of 3

	USAGE
	.\alpha_hide.py -f testalpha.png -s lotdata.txt -o alphaout.png

	.\alpha_unhide.py -f alphaout.png -o alphaout.txt

NEURALNETWORK.PY <br>
	This script was used to train our generative adverserial network. The steganogan package is required to get the script to work (pip install steganogan)

CAPACITIES files <br>
	These files contain the capacity of our alpha method and our visual method

MISCELLANEOUS_CODE (Visual_inspection) <br>
	This notebook contains code used during development.

	Most important is the inspect_lsb function, which you can use the inspect the output.

TEST.PNG <br>
	Contains one file without alpha: test.png and one with alpha channel: testalpha.png used for testing
